import { mental } from '../assets/images/index';
import { blueTone } from '../assets/images/index';
import { colorBoy } from '../assets/images/index';
import { oldTree } from '../assets/images/index';
import { image } from '../assets/images/index';

export const navLinks = [
    {
        id: 1,
        title: 'Home',
        url: '/',
    },
    {
        id: 2,
        title: 'About',
        url: '/about',
    },
    {
        id: 3,
        title: 'Contact',
        url: '/contact',
    },
];
    export const cardContent = [
        {
            id: 1,
            title: 'Mental',
            picture: mental,
        },
        {
            id: 2,
            title: 'Blue Tone',
            picture: blueTone,
        },
        {
            id: 3,
            title: 'Color Boy',
            picture: colorBoy,
        },
        {
            id: 4,
            title: 'Old Tree',
            picture: oldTree,
        },
        {
            id: 5,
            title: 'pic1',
            picture:  image,
        },
        {
            id: 6,
            title: 'pic2',
            picture: image,
        },
        {
            id: 7,
            title: 'pic3',
            picture: image,
        },
        {
            id: 8,
            title: 'pic4',
            picture: image,
        },
        {
            id: 9,
            title: 'pic5',
            picture: image,
        },
        {
            id: 10,
            title: 'pic6',
            picture: image,
        },
    ];

