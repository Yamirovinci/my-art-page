import mental from './Men-tal.jpg';
import blueTone from './Blue-tone.jpg';
import colorBoy from './Color-boy.jpg';
import oldTree from './Old-tree.jpg';
import image from './image.png';
import loading from './loading.gif';

export {
    mental,
    blueTone,
    colorBoy,
    oldTree,
    image,
    loading
};